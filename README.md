# Sudoku

## Description
This is a Sudoku game that randomly picks a Sudoku from a list. You can push a button to check the
Sudoku, get help or generate a new one.



## Authors and acknowledgment
Most of the Sudokus are from
https://gist.github.com/morcefaster/a5a33ee4ea70d4da84f9a3d66c3892e0


## Endpoints
The project currently uses React server
The endpoints for the JSON server is http://localhost:3000/boards
The endpoint for the App is http://localhost:5173/links

## How to run

Go to /src and run "npm run dev"

At the same time in \sudoku run "json-server sudoku.json --port 3000"