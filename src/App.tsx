import { useState, useEffect } from 'react'
import './App.css'
import CheckAnswer from './CheckAnswer';
import axios from 'axios'
import Help from './Help';

function App() {

  function refreshPage() {
    window.location.reload();
  }

  const [numbers, setNumbers] = useState([{ immutable: false, value: 0 }]);
  const [answer, setAnswer] = useState([0]);
  const [randomNumber, setRandomNumber] = useState(0);

  useEffect(() => {


    axios
      .get('http://localhost:3000/boards/')
      .then(response => {
        const newRandomNumber = Math.floor(Math.random() * response.data.length);
        setRandomNumber(newRandomNumber);
        const boardObjects = (response.data[newRandomNumber].board).map(element =>
          (element === "")
            ? { value: element, immutable: false }
            : { value: element, immutable: true }
        )
        setNumbers(boardObjects);
        setAnswer(response.data[newRandomNumber].answer);
      });
  }, []);


  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
      <h1>Sudoku {randomNumber + 1}</h1>
      <div className='sudoku'>
        {numbers.map((number, index) => {
          if (number.immutable) {
            return (
              <input
                key={index}
                id={`quantity-${index}`}
                className={'square'}
                value={number.value}
                disabled // Adding disabled attribute to make the input disabled
                style={{ color: 'blue' }}
              />
            );
          } else {
            return (
              <input
                key={index}
                id={`quantity-${index}`}
                min="1"
                max="9"
                type='number'
                className={'square'}
                value={number.value}
                onChange={(e) => {
                  const newValue = parseInt(e.target.value);
                  const updatedNumbers = [...numbers];
                  updatedNumbers[index].value = newValue;
                  setNumbers(updatedNumbers);
                }}
              />
            );
          }
        })}
      </div >
      <div>
        <CheckAnswer numbers={numbers} answer={answer} />
        <Help numbers={numbers} answer={answer} setNumbers={setNumbers} />
        <button type="button" onClick={refreshPage} style={{
          backgroundColor: 'lightgreen',
          height: '60px', width: '174px'
        }}>
          <span>New Random Sudoku</span> </button>
      </div>
    </div>
  );
}

export default App