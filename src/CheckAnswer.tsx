import compareArrays from "./CompareArrays";


function CheckAnswer(props) {

    const handleClick = () => {
        const isEqual = compareArrays(props.numbers, props.answer);
        if (isEqual) {
            alert("You win!");
        } else {
            alert("There are false answers or empty squares!");
        }
    };

    return (
        <button onClick={handleClick}
            style={{
                backgroundColor: "pink", height: '60px',
                width: '174px'
            }}>
            Check answer</button>
    )
}

export default CheckAnswer;