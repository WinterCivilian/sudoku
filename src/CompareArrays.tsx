function compareArrays(arr1, arr2) {

    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i].value == "") {
            // If any element is not a number return false
            return false;
        }
        if (arr2[i] !== arr1[i].value) {
            return false; // Arrays are not the same
        }
    }
    // If no differences found, arrays are the same
    return true;
}

export default compareArrays;