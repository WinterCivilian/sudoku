import compareArrays from "./CompareArrays";


function Help({ numbers, setNumbers, answer }) {

    function addCorrect() {
        //Do nothing is everything is already correct
        if (compareArrays(numbers, answer)) {
            alert("Everything is correct!");
            return;
        }
        //Array only includs items that are mutable !(immutable)
        const mutableIndices = numbers.reduce((acc, curr, index) => {
            if (!curr.immutable) {
                acc.push(index);
            }
            return acc;
        }, []);

        // Building on top of the preceding part
        // Only include mutables that are not the same as the answers.
        // I.E. empty or false
        const mutableDifferentIndices = mutableIndices.filter((index) =>
            numbers[index].value !== answer[index]);

        // If the array is empty we do nothing
        if (mutableDifferentIndices.length === 0) {
            alert("All mutable values match the answer.");
            return;
        }

        // Pick index randomly
        const randomIndex = mutableDifferentIndices[Math.floor(Math.random() * mutableDifferentIndices.length)];
        const updatedNumbers = [...numbers];
        updatedNumbers[randomIndex].value = answer[randomIndex];
        setNumbers(updatedNumbers);
    }

    const handleClick = () => {
        addCorrect();
    };

    return <button onClick={handleClick}
        style={{ width: '174px', height: '60px', backgroundColor: "lightblue" }}
    >Help</button>;
}

export default Help;